FROM debian:latest

LABEL mantainer="Gius. Camerlingo <gcamerli@gmail.com>"
LABEL version="1.0"
LABEL description="Sftp server for Docker."

# Docker image name
ENV NAME=sftp

# Timezone
ENV TZ="Europe/Paris"

# Update system and install packages
RUN apt-get update
RUN apt-get install -y --no-install-recommends \
	cron \
	iputils-ping
RUN apt-get install -y openssh-server

# Crontab
COPY config/crontab /etc/cron.d/clean-photos
RUN chmod 644 /etc/cron.d/clean-photos

# Clean system
RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Clean ssh keys
RUN mkdir -p /var/run/sshd
RUN rm -f /etc/ssh/ssh_host_*key*

# Copy config
COPY config/sshd_config /etc/ssh/sshd_config
COPY config/entrypoint /

# Cleanup script
COPY config/clean-photos.sh /home/spixel/clean-photos.sh
RUN chmod 744 /home/spixel/clean-photos.sh

# User
RUN useradd -s /bin/bash spixel

# Home
ENV HOME=/home/spixel

# Permissions
RUN chown -R spixel:spixel $HOME

# Healthcheck
COPY healthcheck /usr/local/bin/
RUN chmod 744 /usr/local/bin/healthcheck

# Change user
USER spixel

ENTRYPOINT ["/entrypoint"]
