#!/bin/bash

DIR=/home/spixel/upload

# Cleanup script 
if [ "$(ls -A $DIR)" ]; then
     find -path './upload/*' -delete
     echo "$(date) | Cleanup of $DIR done."
else
    echo "$(date) | Directory $DIR is empty."
fi
