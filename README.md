# **Sftp**

Sftp server for Docker.

### **Credits**

+ [atmoz/sftp](https://github.com/atmoz/sftp)

### **MIT License**

This work is licensed under the terms of **[MIT License](https://bitbucket.org/42spixel/sftp/src/master/LICENSE.md)**.
